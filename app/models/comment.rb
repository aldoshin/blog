class Comment < ActiveRecord::Base
	belongs_to :post
	validates :post_id, presence: true
	validates_presence_of :body
end
